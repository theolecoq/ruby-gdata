Rails.application.routes.draw do
  devise_for :users

  get '/', to: 'posts#index'

  resources :posts, shallow: true do
    resources :comments
  end

  resources :users
end
