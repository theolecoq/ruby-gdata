# README

Bienvenue sur le magnifique projet de rubygdata !

Avant toute chose, lisez bien ces instructions qui vous seront très utile dans votre quête !

Pour lancer le projet :

`bundle install`

`rails db:migrate`

`rails assets:precompile`