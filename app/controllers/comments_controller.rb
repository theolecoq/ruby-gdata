class CommentsController < ApplicationController
    before_action :set_comment, only: [:show, :edit, :update, :destroy]
    before_action :set_post, only: [:new, :create, :edit, :update]

    # GET /comments
    def index
        @comments = Comment.all
    end

    # GET /comments/1
    def show
    end

    # GET /comments/new
    def new
        @comment = Comment.new
    end

    # GET /comments/1/edit
    def edit
    end

    # POST /comments
    def create
        @comment = @post.comments.new(comment_params)
        @comment.user = current_user
        @comment.publication_date = Time.current

        if @comment.save
            redirect_to @post, notice: 'Comment was successfully created.'
        else
            render :new
        end
    end

    # PATCH/PUT /comments/1
    def update
        if @comment.update(comment_params)
            redirect_to @comment, notice: 'Comment was successfully updated.'
        else
            render :edit
        end
    end

    # DELETE /comments/1
    def destroy
        @comment.destroy
        redirect_to @comment.post
    end
end

private

# Use callbacks to share common setup or constraints between actions.
def set_comment
    @comment = Comment.find(params[:id])
    @post = @comment.post
end

# Use callbacks to share common setup or constraints between actions.
def set_post
    @post = Post.find(params[:post_id])
end

# Never trust parameters from the scary internet, only allow the white list through.
def comment_params
    params.require(:comment).permit(:body)
end

